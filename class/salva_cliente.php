<?php
include_once('../parts/header.php');
include_once('../config/conection_db.php');

$nome           = isset($_POST['nome'])           ? $_POST['nome']            : "Campo nome não foi informado!";
$dataNascimento = isset($_POST['dataNascimento']) ? $_POST['dataNascimento']  : "Campo data de nascimento não foi informado!";
$email          = isset($_POST['email'])          ? $_POST['email']           : "Campo email não foi informado!";
$sexo           = isset($_POST['sexo'])           ? $_POST['sexo']            : "Campo sexo não foi informado!";
$cpf            = isset($_POST['cpf'])            ? $_POST['cpf']             : "Campo CPF da não foi informado!";
$celular        = isset($_POST['celular'])        ? $_POST['celular']         : "Campo celular não foi informado!";
$cep            = isset($_POST['cep'])            ? $_POST['cep']             : "Campo cep não foi informado!";
$endereco       = isset($_POST['endereco'])       ? $_POST['endereco']        : "Campo endereço não foi informado!";
$numero         = isset($_POST['numero'])         ? $_POST['numero']          : "Campo número não foi informado!";
$bairro         = isset($_POST['bairro'])         ? $_POST['bairro']          : "Campo bairro não foi informado!";
$cidade         = isset($_POST['cidade'])         ? $_POST['cidade']          : "Campo cidade não foi informado!";
$estado         = isset($_POST['estado'])         ? $_POST['estado']          : "Campo estado não foi informado!";
$complemento    = isset($_POST['complemento'])    ? $_POST['complemento']     : "Campo complemento não foi informado!";
$propaganda     = isset($_POST['propaganda'])     ? $_POST['propaganda']      : "Campo propaganda não foi informado!";
$dataInclusao   = date('Y-m-d H:i:s');

$query = "
    INSERT INTO clientes( nome, dataNascimento, email, sexo, cpf, celular, cep, endereco, numero, bairro, cidade, estado, complemento, dataInclusao )
    VALUES( '$nome', '$dataNascimento', '$email', '$sexo', '$cpf', '$celular', '$cep', '$endereco', '$numero', '$bairro', '$cidade', '$estado', '$complemento', '$dataInclusao' )
    ";

$result = $connection->query($query);

if ($result) {
  print_r('
    <div class="alert alert-success" role="alert">
      Cliente cadastrado com sucesso!
    </div>
    ');
} else {
  print_r('<div class="alert alert-danger" role="alert">
    Erro no cadastro do cliente: '. $nome.'<p>
    data de nascimento: '.$dataNascimento.'<p>
    email:'.$email.'<p>
    sexo:'.$sexo.'<p>
    cpf:'.$cpf.'<p>
    celular:'.$celular.'<p>
    cep:'.$cep.'<p>
    endereco:'.$endereco.'<p>
    numero:'.$numero.'<p>
    bairro:'.$bairro.'<p>
    cidade:'.$cidade.'<p>
    estado:'.$estado .'<p>
    complemento:'.$complemento.'<p>
    propaganda:'.$propaganda.'<p>
    datainclusao: '.$dataInclusao.'<p>
    erro: '.$connection->error.' <p>
    query:'.$query.'
    </div>');
}
include_once('../parts/footer.php');
