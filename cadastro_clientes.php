<?php
include_once('parts/header.php');
?>
<div>
  <div class="shadow-sm p-3 mb-5 bg-white rounded display-3 text-center">Cadastro de Cliente</div>

</div>
<form method="POST" action="class/salva_cliente.php">
  <div class="container-fluid shadow-sm p-3 mb-5 bg-white rounded">

    <label>Dados cadastrais:</label>

    <div class="row">
      <div class="input-group mt-4 col-md-12">
        <div class="input-group-prepend">
          <span class="input-group-text">Nome</span>
        </div>
        <input type="text" class="form-control" name="nome" required>
      </div>
    </div>

    <div class="row">
      <div class="input-group mt-4 col-md-5">
        <div class="input-group-prepend">
          <span class="input-group-text">CPF/CNPJ</span>
        </div>
        <input type="text" class="form-control" name="cpf" minlength="11" maxlength="14" required>
      </div>

      <div class="input-group mt-4 col-md-4">
        <div class="input-group-prepend">
          <span class="input-group-text">Nascimento</span>
        </div>
        <input type="date" class="form-control" placeholder="DD/MM/AAAA" name="dataNascimento" required>
      </div>

      <div class="mt-4 col-md-3">
        <div class="custom-control custom-radio custom-control-inline ">
          <label> </label>
          <input type="radio" id="sexo" value="F" name="sexo" class="custom-control-input">
          <label class="custom-control-label" for="sexo">Feminino</label>
        </div>
        <div class="custom-control custom-radio custom-control-inline ">
          <label> </label>
          <input type="radio" id="customRadioInline2" value="M" name="sexo" class="custom-control-input">
          <label class="custom-control-label" for="customRadioInline2">Masculino</label>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="input-group mt-4 col-md-8">
        <div class="input-group-prepend">
          <span class="input-group-text">E-mail</span>
        </div>
        <input type="text" class="form-control" name="email">
      </div>

      <div class="input-group mt-4 col-md-4">
        <div class="input-group-prepend">
          <span class="input-group-text">Cel:</span>
        </div>
        <input type="text" class="form-control" name="celular" minlength="11" maxlength="11">
      </div>
    </div>

  </div>

  <div class="container-fluid shadow-sm p-3 mb-5 bg-white rounded">
    <label>Dados de localização:</label>

    <div class="row">

      <div class="input-group mt-4 col-md-3">
        <div class="input-group-prepend">
          <span class="input-group-text">CEP:</span>
        </div>
        <input type="text" class="form-control" name="cep" minlength="8" maxlength="8">
      </div>

      <div class="input-group mt-4 col-md-7">
        <div class="input-group-prepend">
          <span class="input-group-text">Endereço</span>
        </div>
        <input type="text" class="form-control" name="endereco">
      </div>

      <div class="input-group mt-4 col-md-2">
        <div class="input-group-prepend">
          <span class="input-group-text">Número</span>
        </div>
        <input type="text" class="form-control" name="numero">
      </div>

    </div>

    <div class="row">

      <div class="input-group mt-4 col-md-3">
        <div class="input-group-prepend">
          <span class="input-group-text">Bairro</span>
        </div>
        <input type="text" class="form-control" name="bairro">
      </div>

      <div class="input-group mt-4 col-md-3">
        <div class="input-group-prepend">
          <span class="input-group-text">Cidade</span>
        </div>
        <input type="text" class="form-control" name="cidade">
      </div>

      <div class="input-group mt-4 col-md-3">
        <div class="input-group-prepend">
          <label class="input-group-text" for="inputGroupSelect01">Estado</label>
        </div>
        <select class="custom-select" id="estado" name="estado">
          <option value="AC">Acre</option>
          <option value="AL">Alagoas</option>
          <option value="AP">Amapá</option>
          <option value="AM">Amazonas</option>
          <option value="BA">Bahia</option>
          <option value="CE">Ceará</option>
          <option value="DF">Distrito Federal</option>
          <option value="ES">Espírito Santo</option>
          <option value="GO">Goiás</option>
          <option value="MA">Maranhão</option>
          <option value="MT">Mato Grosso</option>
          <option value="MS">Mato Grosso do Sul</option>
          <option value="MG">Minas Gerais</option>
          <option value="PA">Pará</option>
          <option value="PB">Paraíba</option>
          <option value="PR">Paraná</option>
          <option value="PE">Pernambuco</option>
          <option value="PI">Piauí</option>
          <option value="RJ">Rio de Janeiro</option>
          <option value="RN">Rio Grande do Norte</option>
          <option value="RS">Rio Grande do Sul</option>
          <option value="RO">Rondônia</option>
          <option value="RR">Roraima</option>
          <option value="SC">Santa Catarina</option>
          <option value="SP">São Paulo</option>
          <option value="SE">Sergipe</option>
          <option value="TO">Tocantins</option>
          <option value="EX">Estrangeiro</option>
        </select>
      </div>

      <div class="input-group mt-4 col-md-3">
        <div class="input-group-prepend">
          <span class="input-group-text">complemento</span>
        </div>
        <input type="text" class="form-control" name="complemento">
      </div>

    </div>
  </div>

  <div class="container-fluid shadow-sm p-3 mb-5 bg-white rounded">
    <div class="row col-md-12">

      <div class="custom-control custom-switch">
        <input type="checkbox" class="custom-control-input" id="customSwitch1" name="propaganda">
        <label class="custom-control-label" for="customSwitch1">Receber propagandas ?</label>
      </div>

    </div>

    <div class="row mt-4 col-md-12">
      <button type="submit" class="btn btn-success  btn-lg btn-block">Enviar Formulario</button>
    </div>

  </div>

</form>
<?php
include_once('parts/footer.php');
?>